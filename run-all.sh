#! /usr/bin/env bash

set -e

# it must build
python check-build.py --name seedflow --version 0.3.0
python check-imports.py
vulture setup.py
# it must behave correctly
python run-tests.py test.py
# it must not be obviously insecure
bandit -r .
# it must be consistently defined
MYPYPATH=../rototiller:../din mypy --follow-imports silent --ignore-missing-imports .
# it must be conformant to community standards
flake8 --exclude '*/test*.py,test*.py' --config .flake8
flake8 --filename '*/test*.py,test*.py' --config .flake8.test
pylint *.py
isort -rc . --check-only --diff -lai 2
python find-commented-code.py
