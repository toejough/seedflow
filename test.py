"""Tests for seedflow."""


# [ Imports:Python ]
import pprint
import signal
import sys
import time
import types
import typing

# [ Imports:Third Party ]
import din
import pipe

# [ Imports:Project ]
import seedflow


# intentionally catching broad exceptions for testing
# pylint: disable=broad-except
# intentionally long names allowed
# pylint: disable=invalid-name
# not willing to split up this test file yet
# pylint: disable=too-many-lines


@pipe.Pipe  # type: ignore
def is_multiline(string: str) -> bool:
    return 1 < len(string.splitlines())


def indent(string: str) -> str:
    return '  ' + '\n  '.join(string.splitlines())


def limit_lines(string: str, *, max_lines: int) -> str:
    lines = string.splitlines()
    allowed_lines = lines[:max_lines]
    lines_left = len(lines) - len(allowed_lines)
    if lines_left:
        allowed_lines.append(f"<<<< +{lines_left} more >>>>")
    return '\n'.join(allowed_lines)


def _format_assertion(**message_kwargs: typing.Any) -> str:
    message = f"Assertion failed!"
    for key, value in message_kwargs.items():
        value_string = f"{value}"
        if value_string | is_multiline:
            value_string = limit_lines(value_string, max_lines=20)
            value_string = "\n" + indent(value_string)
        message += f"\n{key}: {value_string}"
    return message


def _raise_assertion(**message_kwargs: typing.Any) -> None:
    message = _format_assertion(**message_kwargs)
    raise AssertionError(message)


def _assert(success: bool, **message_kwargs: typing.Any) -> None:
    if not success:
        _raise_assertion(**message_kwargs)


# [ Behavior Tests ]
def test_recursion_limit_hit() -> None:
    # Given
    async def limited_factorial(number: int, *, acc: int = 1) -> int:
        if number < 1:
            return acc
        return (await limited_factorial(number - 1, acc=acc * number))

    actual_error_type: typing.Optional[typing.Type[Exception]] = None
    expected_error_type = RecursionError

    # When
    try:
        seedflow.run_sync(limited_factorial, 10000)
    # intentionally broad exception handling
    except Exception as error:  # pylint: disable=broad-except
        actual_error_type = type(error)

    # Then
    assert actual_error_type == expected_error_type, f"actual ({actual_error_type}) != expected ({expected_error_type})"


def test_recursion_limit_not_hit() -> None:
    # Given
    async def unlimited_factorial(number: int, *, acc: int = 1) -> int:
        if number < 1:
            return acc
        raise seedflow.TailCall(unlimited_factorial, number - 1, acc=acc * number)

    actual_error_type: typing.Optional[typing.Type[Exception]] = None
    expected_error_type = None

    # When
    try:
        seedflow.run_sync(unlimited_factorial, 10000)
    # intentionally broad exception handling
    except Exception as error:  # pylint: disable=broad-except
        actual_error_type = type(error)

    # Then
    assert actual_error_type == expected_error_type, f"actual ({actual_error_type}) != expected ({expected_error_type})"


def test_tail_recursion_in_normal_func() -> None:
    # Given
    def normal_func(number: int, *, result: int = 1) -> int:
        if number < 1:
            return result
        raise seedflow.TailCall(normal_func, number - 1, result=result * number)

    async def async_func(number: int) -> None:
        return (await seedflow.run(normal_func, number))

    actual_error_type: typing.Optional[typing.Type[Exception]] = None
    expected_error_type = None
    expected_result = 3628800

    # When
    try:
        result = seedflow.run_sync(async_func, 10)
    # intentionally broad exception handling
    except Exception as error:  # pylint: disable=broad-except
        actual_error_type = type(error)

    # Then
    assert actual_error_type == expected_error_type, f"actual ({actual_error_type}) != expected ({expected_error_type})"
    assert result == expected_result, f"actual ({result}) != expected ({expected_result})"


def test_tail_recursion_with_normal_final_func() -> None:
    # Given
    initial_value = 10
    expected_result = 10

    def normal_func(number: int) -> int:
        return number

    async def async_func(number: int) -> None:
        raise seedflow.TailCall(normal_func, number)

    # When
    result = seedflow.run_sync(async_func, initial_value)

    # Then
    _assert(
        # not sure why pylint thinks this is a callable, but this could be a problem...
        result == expected_result,  # pylint: disable=comparison-with-callable
        message="actual != expected",
        actual=result,
        expected=expected_result,
    )


def test_function_tasks_get_called() -> None:
    # Given
    actual_record = {
        'called': False,
        'args': (),
        'kwargs': {},
    }
    expected_record = {
        'called': True,
        'args': ('foo', 'bar'),
        'kwargs': {'baz': 'quux'},
    }

    def _function(*args: typing.Any, **kwargs: typing.Any) -> None:
        actual_record['called'] = True
        actual_record['args'] = args
        actual_record['kwargs'] = kwargs

    async def _run_function_tasks() -> None:
        await seedflow.run(_function, 'foo', 'bar', baz='quux')

    # When
    seedflow.run_sync(_run_function_tasks)

    # Then
    _assert(actual_record == expected_record, actual_record=actual_record, expected_record=expected_record)


def test_function_tasks_results_get_returned() -> None:
    # Given
    actual_record = {
        'called': False,
        'args': (),
        'kwargs': {},
        'returned': None,
    }
    expected_record = {
        'called': True,
        'args': ('foo', 'bar'),
        'kwargs': {'baz': 'quux'},
        'returned': 'RETURN VALUE',
    }

    def _function(*args: typing.Any, **kwargs: typing.Any) -> str:
        actual_record['called'] = True
        actual_record['args'] = args
        actual_record['kwargs'] = kwargs
        return 'RETURN VALUE'

    async def _run_function_tasks() -> None:
        actual_record['returned'] = await seedflow.run(_function, 'foo', 'bar', baz='quux')

    # When
    seedflow.run_sync(_run_function_tasks)

    # Then
    _assert(actual_record == expected_record, actual_record=actual_record, expected_record=expected_record)


class _ExceptionRecord(din.EqualityMixin, din.ReprMixin):
    def __init__(
        self, *,
        type_: typing.Optional[typing.Type[BaseException]] = None,
        string: str = '',
        function_names: typing.Tuple[str, ...] = (),
    ) -> None:
        super().__init__()
        self.type_ = type_
        self.string = string
        self.function_names = function_names
        # Vulture
        self.type_, self.string, self.function_names  # pylint: disable=pointless-statement


def test_function_errors_thrown_to_caller() -> None:
    # Given
    actual_record = _ExceptionRecord()
    expected_record = _ExceptionRecord(
        type_=RuntimeError,
        string='for testing',
        function_names=(
            'test_function_errors_thrown_to_caller',
            'run_sync',
            '_run_function_tasks',
            '_function',
        ),
    )

    def _function(*args: typing.Any, **kwargs: typing.Any) -> None:
        raise RuntimeError("for testing")

    async def _run_function_tasks() -> None:
        await seedflow.run(_function, 'foo', 'bar', baz='quux')

    # When
    try:
        seedflow.run_sync(_run_function_tasks)
    except RuntimeError:
        actual_record.type_, error, backtrace = sys.exc_info()
        actual_record.string = str(error)
        traceback_obj = backtrace
        while traceback_obj:
            function_name = traceback_obj.tb_frame.f_code.co_name
            actual_record.function_names = (*actual_record.function_names, function_name)
            traceback_obj = traceback_obj.tb_next

    # Then
    _assert(actual_record == expected_record, actual_record=pprint.pformat(actual_record), expected_record=pprint.pformat(expected_record))


def test_awaitable_tasks_get_called() -> None:
    # Given
    actual_record = {
        'called': False,
        'args': (),
        'kwargs': {},
    }
    expected_record = {
        'called': True,
        'args': ('foo', 'bar'),
        'kwargs': {'baz': 'quux'},
    }

    async def _awaitable(*args: typing.Any, **kwargs: typing.Any) -> None:
        actual_record['called'] = True
        actual_record['args'] = args
        actual_record['kwargs'] = kwargs

    async def _run_awaitable_tasks() -> None:
        await seedflow.run(_awaitable, 'foo', 'bar', baz='quux')

    # When
    seedflow.run_sync(_run_awaitable_tasks)

    # Then
    _assert(actual_record == expected_record, actual_record=actual_record, expected_record=expected_record)


def test_awaitable_tasks_results_get_returned() -> None:
    # Given
    actual_record = {
        'called': False,
        'args': (),
        'kwargs': {},
        'returned': None,
    }
    expected_record = {
        'called': True,
        'args': ('foo', 'bar'),
        'kwargs': {'baz': 'quux'},
        'returned': 'RETURN VALUE',
    }

    async def _awaitable(*args: typing.Any, **kwargs: typing.Any) -> str:
        actual_record['called'] = True
        actual_record['args'] = args
        actual_record['kwargs'] = kwargs
        return 'RETURN VALUE'

    async def _run_awaitable_tasks() -> None:
        actual_record['returned'] = await seedflow.run(_awaitable, 'foo', 'bar', baz='quux')

    # When
    seedflow.run_sync(_run_awaitable_tasks)

    # Then
    _assert(actual_record == expected_record, actual_record=actual_record, expected_record=expected_record)


def test_awaitable_errors_thrown_to_caller() -> None:
    # Given
    actual_record = _ExceptionRecord()
    expected_record = _ExceptionRecord(
        type_=RuntimeError,
        string='for testing',
        function_names=(
            'test_awaitable_errors_thrown_to_caller',
            'run_sync',
            '_run_awaitable_tasks',
            '_awaitable',
        ),
    )

    def _awaitable(*args: typing.Any, **kwargs: typing.Any) -> None:
        raise RuntimeError("for testing")

    async def _run_awaitable_tasks() -> None:
        await seedflow.run(_awaitable, 'foo', 'bar', baz='quux')

    # When
    try:
        seedflow.run_sync(_run_awaitable_tasks)
    except RuntimeError:
        actual_record.type_, error, backtrace = sys.exc_info()
        actual_record.string = str(error)
        traceback_obj = backtrace
        while traceback_obj:
            function_name = traceback_obj.tb_frame.f_code.co_name
            actual_record.function_names = (*actual_record.function_names, function_name)
            traceback_obj = traceback_obj.tb_next

    # Then
    _assert(actual_record == expected_record, actual_record=pprint.pformat(actual_record), expected_record=pprint.pformat(expected_record))


def test_nested_tasks_get_called() -> None:
    # Given
    actual_record = {
        'called': False,
        'args': (),
        'kwargs': {},
    }
    expected_record = {
        'called': True,
        'args': ('foo', 'bar'),
        'kwargs': {'baz': 'quux'},
    }

    async def _nested_awaitable(*args: typing.Any, **kwargs: typing.Any) -> None:
        actual_record['called'] = True
        actual_record['args'] = args
        actual_record['kwargs'] = kwargs

    async def _awaitable(*args: typing.Any, **kwargs: typing.Any) -> None:
        await seedflow.run(_nested_awaitable, *args, **kwargs)

    async def _run_awaitable_tasks() -> None:
        await seedflow.run(_awaitable, 'foo', 'bar', baz='quux')

    # When
    seedflow.run_sync(_run_awaitable_tasks)

    # Then
    _assert(actual_record == expected_record, actual_record=actual_record, expected_record=expected_record)


def test_nested_tasks_results_get_returned() -> None:
    # Given
    actual_record = {
        'called': False,
        'args': (),
        'kwargs': {},
        'returned': None,
    }
    expected_record = {
        'called': True,
        'args': ('foo', 'bar'),
        'kwargs': {'baz': 'quux'},
        'returned': 'RETURN VALUE',
    }

    async def _nested_awaitable(*args: typing.Any, **kwargs: typing.Any) -> str:
        actual_record['called'] = True
        actual_record['args'] = args
        actual_record['kwargs'] = kwargs
        return 'RETURN VALUE'

    async def _awaitable(*args: typing.Any, **kwargs: typing.Any) -> str:
        return await seedflow.run(_nested_awaitable, *args, **kwargs)

    async def _run_awaitable_tasks() -> None:
        actual_record['returned'] = await seedflow.run(_awaitable, 'foo', 'bar', baz='quux')

    # When
    seedflow.run_sync(_run_awaitable_tasks)

    # Then
    _assert(actual_record == expected_record, actual_record=actual_record, expected_record=expected_record)


def test_nested_task_errors_thrown_through_stack() -> None:
    # Given
    actual_record = _ExceptionRecord()
    expected_record = _ExceptionRecord(
        type_=RuntimeError,
        string='for testing',
        function_names=(
            'test_nested_task_errors_thrown_through_stack',
            'run_sync',
            '_run_awaitable_tasks',
            '_awaitable',
            '_nested_awaitable',
        ),
    )

    async def _nested_awaitable(*args: typing.Any, **kwargs: typing.Any) -> None:
        raise RuntimeError("for testing")

    async def _awaitable(*args: typing.Any, **kwargs: typing.Any) -> None:
        return await seedflow.run(_nested_awaitable, *args, **kwargs)

    async def _run_awaitable_tasks() -> None:
        await seedflow.run(_awaitable, 'foo', 'bar', baz='quux')

    # When
    try:
        seedflow.run_sync(_run_awaitable_tasks)
    except RuntimeError:
        actual_record.type_, error, backtrace = sys.exc_info()
        actual_record.string = str(error)
        traceback_obj = backtrace
        while traceback_obj:
            function_name = traceback_obj.tb_frame.f_code.co_name
            actual_record.function_names = (*actual_record.function_names, function_name)
            traceback_obj = traceback_obj.tb_next

    # Then
    _assert(actual_record == expected_record, actual_record=pprint.pformat(actual_record), expected_record=pprint.pformat(expected_record))


def test_running_decorated_function() -> None:
    # Given
    expected_result = 'return value'

    @seedflow.as_sync
    async def _decorated() -> str:
        return await seedflow.run(lambda: expected_result)

    # When
    result = _decorated()

    # Then
    _assert(
        result == expected_result,
        message="The decorated function didn't return the expected value when run.",
        actual=result,
        expected=expected_result,
    )


def test_running_till_all_with_mixed_tasks() -> None:
    # Given
    async1_finished = False
    sync1_finished = False

    async def async1() -> None:
        nonlocal async1_finished
        async1_finished = True

    async def sync1() -> None:
        nonlocal sync1_finished
        sync1_finished = True

    async def all_with_async_tasks() -> None:
        task1 = seedflow.Task(async1)
        task2 = seedflow.Task(sync1)
        await seedflow.till_all(task1, task2)

    # When
    seedflow.run_sync(all_with_async_tasks)

    # Then
    _assert(async1_finished, message="async1 func didn't finish!")
    _assert(sync1_finished, message="sync1 func didn't finish!")


def test_running_till_all_with_sync_tasks_returns_results() -> None:
    # Given
    expected_results = (seedflow.Value('sync1 result'), seedflow.Value('sync2 result'))

    def sync1() -> str:
        return 'sync1 result'

    def sync2() -> str:
        return 'sync2 result'

    async def all_with_sync_tasks() -> typing.Tuple[seedflow.ReturnType, ...]:
        task1 = seedflow.Task(sync1)
        task2 = seedflow.Task(sync2)
        return await seedflow.till_all(task1, task2)

    # When
    actual_results = seedflow.run_sync(all_with_sync_tasks)

    # Then
    _assert(
        actual_results == expected_results,
        message="sync results different than expected!",
        actual_results=actual_results,
        expected_results=expected_results,
    )


def test_running_till_all_with_async_tasks_returns_results() -> None:
    # Given
    expected_results = (seedflow.Value('async1 result'), seedflow.Value('async2 result'))

    async def async1() -> str:
        return 'async1 result'

    async def async2() -> str:
        return 'async2 result'

    async def all_with_async_tasks() -> typing.Tuple[seedflow.ReturnType, ...]:
        task1 = seedflow.Task(async1)
        task2 = seedflow.Task(async2)
        return await seedflow.till_all(task1, task2)

    # When
    actual_results = seedflow.run_sync(all_with_async_tasks)

    # Then
    _assert(
        actual_results == expected_results,
        message="async results different than expected!",
        actual_results=actual_results,
        expected_results=expected_results,
    )


def test_running_till_all_with_mixed_tasks_returns_results() -> None:
    # Given
    expected_results = (seedflow.Value('async1 result'), seedflow.Value('sync1 result'))

    async def async1() -> str:
        return 'async1 result'

    def sync1() -> str:
        return 'sync1 result'

    async def all_with_mixed_tasks() -> typing.Tuple[seedflow.ReturnType, ...]:
        task1 = seedflow.Task(async1)
        task2 = seedflow.Task(sync1)
        return await seedflow.till_all(task1, task2)

    # When
    actual_results = seedflow.run_sync(all_with_mixed_tasks)

    # Then
    _assert(
        actual_results == expected_results,
        message="mixed results different than expected!",
        actual_results=actual_results,
        expected_results=expected_results,
    )


def test_running_till_all_with_mixed_tasks_and_first_error() -> None:
    # Given
    expected_results = (
        seedflow.Error(RuntimeError, RuntimeError('async error'), None),
        seedflow.Value('sync1 result'),
    )

    async def async1() -> str:
        raise RuntimeError('async error')

    def sync1() -> str:
        return 'sync1 result'

    async def all_with_mixed_tasks() -> typing.Tuple[seedflow.ReturnType, ...]:
        task1 = seedflow.Task(async1)
        task2 = seedflow.Task(sync1)
        return await seedflow.till_all(task1, task2)

    # When
    actual_results = seedflow.run_sync(all_with_mixed_tasks)

    # Then
    _assert(
        isinstance(actual_results[0], seedflow.Error),
        message="First result was not an Error!",
        actual_results=type(actual_results[0]),
        expected_results='seedflow.Error',
    )
    _assert(
        actual_results[0].exc_type == expected_results[0].exc_type,
        message="mixed results different than expected!",
        actual_results=actual_results[0].exc_type,
        expected_results=expected_results[0].exc_type,
    )
    _assert(
        str(actual_results[0].exc_value) == str(expected_results[0].exc_value),
        message="mixed results[0] different than expected!",
        actual_results=actual_results[0].exc_value,
        expected_results=expected_results[0].exc_value,
    )
    _assert(
        actual_results[1] == expected_results[1],
        message="mixed results[1] different than expected!",
        actual_results=actual_results[1],
        expected_results=expected_results[1],
    )
    _assert(
        len(actual_results) == len(expected_results),
        message="mixed results length different than expected!",
        actual_results=len(actual_results),
        expected_results=len(expected_results),
    )


async def _async_nop():
    pass


def test_running_till_all_with_async_tasks() -> None:
    # Given
    hunks_run = []
    expected_hunks_run = ['1a', '2a', '3a', '1b', '2b', '2c']
    expected_results = (seedflow.Value(1), seedflow.Value(2), seedflow.Error(None, RuntimeError('async3 error'), None))

    async def record(hunk) -> None:
        hunks_run.append(hunk)
        return await seedflow.run(_async_nop)

    async def error(message) -> typing.NoReturn:
        raise RuntimeError(message)

    async def async1() -> int:
        await record('1a')
        await record('1b')
        return 1

    async def async2() -> int:
        await record('2a')
        await record('2b')
        await record('2c')
        return 2

    async def async3() -> None:
        await record('3a')
        await seedflow.run(error, "async3 error")

    async def all_with_async_tasks() -> typing.Tuple[seedflow.ReturnType, ...]:
        task1 = seedflow.Task(async1)
        task2 = seedflow.Task(async2)
        task3 = seedflow.Task(async3)
        return await seedflow.till_all(task1, task2, task3)

    # When
    results = seedflow.run_sync(all_with_async_tasks)

    # Then
    _assert(
        expected_hunks_run == hunks_run,
        message="hunks run were different than expected!",
        hunks_run=hunks_run,
        expected_hunks_run=expected_hunks_run,
    )
    _assert(
        expected_results == results,
        message="results differed.",
        results=results,
        expected_results=expected_results,
    )


def test_running_till_all_with_sync_tasks() -> None:
    # Given
    hunks_run = []
    expected_hunks_run = ['1a', '2a', '3a']
    expected_results = (seedflow.Value(1), seedflow.Value(2), seedflow.Error(None, RuntimeError('sync3 error'), None))

    def record(hunk) -> None:
        hunks_run.append(hunk)

    def error(message) -> typing.NoReturn:
        raise RuntimeError(message)

    def sync1() -> int:
        record('1a')
        return 1

    def sync2() -> int:
        record('2a')
        return 2

    def sync3() -> None:
        record('3a')
        error("sync3 error")

    async def all_with_sync_tasks():
        task1 = seedflow.Task(sync1)
        task2 = seedflow.Task(sync2)
        task3 = seedflow.Task(sync3)
        return await seedflow.till_all(task1, task2, task3)

    # When
    results = seedflow.run_sync(all_with_sync_tasks)

    # Then
    _assert(
        expected_hunks_run == hunks_run,
        message="hunks run were different than expected!",
        hunks_run=hunks_run,
        expected_hunks_run=expected_hunks_run,
    )
    _assert(
        expected_results == results,
        message="results differed.",
        results=results,
        expected_results=expected_results,
    )


def test_running_till_all_with_sub_till_alls() -> None:
    # Given
    hunks_run = []
    expected_hunks_run = ['1a', '3a', '2a', '1b', '2b', '4a', '6a', '2c', '5a', '4b', '5b', '5c']
    expected_results = (
        seedflow.Value(1), seedflow.Value(2), seedflow.Value(3),
        seedflow.Value(
            (seedflow.Value(4), seedflow.Value(5), seedflow.Value(6)),
        ),
    )

    def record(hunk) -> None:
        hunks_run.append(hunk)

    async def async1() -> int:
        await seedflow.run(record, '1a')
        await seedflow.run(record, '1b')
        return 1

    async def async2() -> int:
        await seedflow.run(record, '2a')
        await seedflow.run(record, '2b')
        await seedflow.run(record, '2c')
        return 2

    def sync3() -> int:
        record('3a')
        return 3

    async def async4() -> int:
        await seedflow.run(record, '4a')
        await seedflow.run(record, '4b')
        return 4

    async def async5() -> int:
        await seedflow.run(record, '5a')
        await seedflow.run(record, '5b')
        await seedflow.run(record, '5c')
        return 5

    def sync6() -> int:
        record('6a')
        return 6

    async def bottom_level_all():
        return await seedflow.till_all(
            seedflow.Task(async4),
            seedflow.Task(async5),
            seedflow.Task(sync6),
        )

    async def top_level_all():
        return await seedflow.till_all(
            seedflow.Task(async1),
            seedflow.Task(async2),
            seedflow.Task(sync3),
            seedflow.Task(bottom_level_all),
        )

    # When
    results = seedflow.run_sync(top_level_all)

    # Then
    _assert(
        expected_hunks_run == hunks_run,
        message="hunks run were different than expected!",
        hunks_run=hunks_run,
        expected_hunks_run=expected_hunks_run,
    )
    _assert(
        expected_results == results,
        message="results differed.",
        results=results,
        expected_results=expected_results,
    )


def test_running_till_any_with_sync_tasks() -> None:
    # Given
    hunks_run = []

    def record(hunk) -> None:
        hunks_run.append(hunk)

    def error(message) -> typing.NoReturn:
        raise RuntimeError(message)

    def sync1() -> int:
        record('1a')
        return 1

    def sync2() -> int:
        record('2a')
        return 2

    def sync3() -> None:
        record('3a')
        error("sync3 error")

    async def any_with_sync_tasks():
        task1 = seedflow.Task(sync1)
        task2 = seedflow.Task(sync2)
        task3 = seedflow.Task(sync3)
        return await seedflow.till_any(task1, task2, task3)

    # When
    results = seedflow.run_sync(any_with_sync_tasks)

    # Then
    _assert(
        '1a' in hunks_run,
        message="Expected 1a to be in hunks",
        hunks_run=hunks_run,
    )
    _assert(
        seedflow.Value(1) in results,
        message="expected seedflow.Value(1) to be in results",
        results=results,
    )


def test_running_till_any_with_async_tasks() -> None:
    # Given
    hunks_run = []
    expected_hunks_run = ['1a', '2a', '3a', '1b']
    expected_results = (seedflow.Cancelled(), seedflow.Value(2), seedflow.Cancelled())

    async def record(hunk) -> None:
        hunks_run.append(hunk)
        return await seedflow.run(_async_nop)

    async def error(message) -> typing.NoReturn:
        raise RuntimeError(message)

    async def async1() -> int:
        await record('1a')
        await record('1b')
        return 1

    async def async2() -> int:
        await record('2a')
        return 2

    async def async3() -> None:
        await record('3a')
        await seedflow.run(error, "async3 error")

    async def all_with_async_tasks() -> typing.Tuple[seedflow.ReturnType, ...]:
        task1 = seedflow.Task(async1)
        task2 = seedflow.Task(async2)
        task3 = seedflow.Task(async3)
        return await seedflow.till_any(task1, task2, task3)

    # When
    results = seedflow.run_sync(all_with_async_tasks)

    # Then
    _assert(
        expected_hunks_run == hunks_run,
        message="hunks run were different than expected!",
        hunks_run=hunks_run,
        expected_hunks_run=expected_hunks_run,
    )
    _assert(
        expected_results == results,
        message="results differed.",
        results=results,
        expected_results=expected_results,
    )


_isinstance_p = pipe.Pipe(isinstance)


def test_running_till_predicate() -> None:
    # Given
    hunks_run = []
    expected_hunks_run = ['1a', '2a', '3a', '1b', '3b', '1c', '1d', '1e']
    expected_results = (seedflow.Cancelled(), seedflow.Value(2), seedflow.Error(None, RuntimeError(3), None))

    async def record(hunk) -> None:
        hunks_run.append(hunk)
        return await seedflow.run(_async_nop)

    async def error(message) -> typing.NoReturn:
        raise RuntimeError(message)

    async def async1() -> int:
        await record('1a')
        await record('1b')
        await record('1c')
        await record('1d')
        await record('1e')
        await record('1f')
        await record('1g')
        await record('1h')
        return 1

    async def async2() -> int:
        await record('2a')
        return 2

    async def async3() -> None:
        await record('3a')
        await record('3b')
        await seedflow.run(error, "async3 error")

    def error_result(results):
        return any(r | _isinstance_p(seedflow.Error) for r in results.values())

    async def all_with_async_tasks() -> typing.Tuple[seedflow.ReturnType, ...]:
        task1 = seedflow.Task(async1)
        task2 = seedflow.Task(async2)
        task3 = seedflow.Task(async3)
        return await seedflow.till(error_result, task1, task2, task3)

    # When
    results = seedflow.run_sync(all_with_async_tasks)

    # Then
    _assert(
        expected_hunks_run == hunks_run,
        message="hunks run were different than expected!",
        hunks_run=hunks_run,
        expected_hunks_run=expected_hunks_run,
    )
    _assert(
        expected_results == results,
        message="results differed.",
        results=results,
        expected_results=expected_results,
    )


def test_on_timeout_once() -> None:
    # Given
    messages = []
    expected_messages = [0.5, 1, 1.5, 2.1]
    expected_results = (
        seedflow.Cancelled(),
        seedflow.Value('logged 2.1'),
        seedflow.Value('logged 1.5'),
        seedflow.Value('logged 1'),
        seedflow.Value('logged 0.5'),
        seedflow.Value(None),
    )

    def log(this_message):
        messages.append(this_message)
        return f'logged {this_message}'

    @pipe.Pipe
    def seconds_passed(num_seconds):
        start = time.time()

        def _seconds_passed(_results):
            return start + num_seconds < time.time()

        return _seconds_passed

    async def async_func():
        return await seedflow.till(
            2 | seconds_passed,
            seedflow.Task(seedflow.on_timeout, 2.5, log, 2.5),
            seedflow.Task(seedflow.on_timeout, 2.1, log, 2.1),
            seedflow.Task(seedflow.on_timeout, 1.5, log, 1.5),
            seedflow.Task(seedflow.on_timeout, 1, log, 1),
            seedflow.Task(seedflow.on_timeout, 0.5, log, 0.5),
            seedflow.Task(seedflow.on_timeout, 0),
        )

    # When
    results = seedflow.run_sync(async_func)

    # Then
    _assert(
        messages == expected_messages,
        message="messages logged on timeout are different than expected!",
        messages=messages,
        expected_messages=expected_messages,
    )
    _assert(
        results == expected_results,
        message="results are not as expected!",
        results=results,
        expected_results=expected_results,
    )


def test_on_signal_once() -> None:
    # Given
    messages = []
    expected_messages = ['alarm']
    expected_results = (
        seedflow.Cancelled(),
        seedflow.Value('logged alarm'),
        seedflow.Value(None),
    )

    def log(this_message):
        messages.append(this_message)
        return f'logged {this_message}'

    def send_alarm(when):
        signal.alarm(when)

    def two_done(results):
        return 2 <= len([r for r in results.values() if not r | _isinstance_p(seedflow.Spawned)])

    async def async_func():
        return await seedflow.till(
            two_done,
            seedflow.Task(seedflow.on_signal, signal.SIGINT, log, 'sig int!'),
            seedflow.Task(seedflow.on_signal, signal.SIGALRM, log, 'alarm'),
            seedflow.Task(send_alarm, 1),
        )

    # When
    results = seedflow.run_sync(async_func)

    # Then
    _assert(
        messages == expected_messages,
        message="messages logged on signal are different than expected!",
        messages=messages,
        expected_messages=expected_messages,
    )
    _assert(
        results == expected_results,
        message="results are not as expected!",
        results=results,
        expected_results=expected_results,
    )


def test_on_interval() -> None:
    # Given
    messages = []
    expected_messages = [0.5, 0.5, 0.5, 0.5, 2.1]
    expected_results = (
        seedflow.Cancelled(),
        seedflow.Value('logged 2.1'),
    )

    def log(this_message):
        messages.append(this_message)
        return f'logged {this_message}'

    async def async_func():
        return await seedflow.till_any(
            seedflow.Task(seedflow.on_interval, 0.5, log, 0.5),
            seedflow.Task(seedflow.on_timeout, 2.1, log, 2.1),
        )

    # When
    results = seedflow.run_sync(async_func)

    # Then
    _assert(
        messages == expected_messages,
        message="messages logged on timeout are different than expected!",
        messages=messages,
        expected_messages=expected_messages,
    )
    _assert(
        results == expected_results,
        message="results are not as expected!",
        results=results,
        expected_results=expected_results,
    )


def test_on_signal_forever() -> None:
    # Given
    messages = []
    expected_messages = ['alarm', 'alarm', 'alarm']
    expected_results = (
        seedflow.Cancelled(),
        seedflow.Value(None),
        seedflow.Value(None),
    )

    def log(this_message):
        messages.append(this_message)
        return f'logged {this_message}'

    async def stop_alarm_after(num):
        while len(messages) < num:
            await seedflow.run(lambda: None)
        signal.setitimer(signal.ITIMER_REAL, 0, 0)

    def send_repeating_alarm(interval):
        signal.setitimer(signal.ITIMER_REAL, 0.5, 0.5)

    def two_done(results):
        return 2 <= len([r for r in results.values() if not r | _isinstance_p(seedflow.Spawned)])

    async def async_func():
        return await seedflow.till(
            two_done,
            seedflow.Task(seedflow.on_signal_forever, signal.SIGALRM, log, 'alarm'),
            seedflow.Task(send_repeating_alarm, 1),
            seedflow.Task(stop_alarm_after, 3),
        )

    # When
    results = seedflow.run_sync(async_func)

    # Then
    _assert(
        results == expected_results,
        message="results are not as expected!",
        results=results,
        expected_results=expected_results,
    )
    _assert(
        messages == expected_messages,
        message="messages logged on signal are different than expected!",
        messages=messages,
        expected_messages=expected_messages,
    )


# this is for a very specific test case that uses 5 funcs, and a log func
# allowing this here because it's still clear use - refactoring would only
# be about appeasing the linter without actually improving the code.
async def _async_func(func_a, func_b, func_c, func_d, func_e, log):  # pylint: disable=too-many-arguments
    # a -> c  (call c with a's output - c is dependent only on a to start running)
    # b -> d  (call d with b's output - d is dependent only on b to start running)
    # a, b, c -> e  (call e with a, b, and c's outputs, - e is dependent only on those three to start running)
    # return d, e  (return once d and e are both ready)
    state = {
        'a': {'id': None, 'result': None},
        'b': {'id': None, 'result': None},
        'c': {'id': None, 'result': None},
        'd': {'id': None, 'result': None},
        'e': {'id': None, 'result': None},
    }
    state['a']['id'] = await seedflow.spawn(func_a, log)
    state['b']['id'] = await seedflow.spawn(func_b, log)
    while not all(state[n]['result'] for n in ('d', 'e')):
        results = await seedflow.get_results()
        for sub_state in state.values():
            if sub_state['id'] in results:
                sub_state['result'] = results[sub_state['id']]
        if state['a']['result'] and not state['c']['id']:
            state['c']['id'] = await seedflow.spawn(func_c, state['a']['result'], log)
        if state['b']['result'] and not state['d']['id']:
            state['d']['id'] = await seedflow.spawn(func_d, state['b']['result'], log)
        if all(state[n]['result'] for n in ('a', 'b', 'c')) and not state['e']['id']:
            state['e']['id'] = await seedflow.spawn(
                func_e,
                state['a']['result'],
                state['b']['result'],
                state['c']['result'],
                log,
            )
    return state['d']['result'], state['e']['result']


async def _log_a(log):
    log('a')
    return f'a result'


async def _log_b(log):
    log('b')
    return f'b result'


async def _log_c(other_result, log):
    log('c')
    return f'{other_result} and c result'


async def _log_d(other_result, log):
    log('d')
    return f'{other_result} and d result'


async def _log_e(other_1, other_2, other_3, log):
    log('e')
    return f'{other_1}, {other_2}, {other_3}, and e result'


def _long(func):
    async def _long_func(*args):
        for _ in range(50):
            await seedflow.run(lambda: None)
        return await seedflow.run(func, *args)
    return _long_func


def test_complex_flow_long_a() -> None:
    # Given
    actual_log = []

    def log(message):
        actual_log.append(message)

    expected_log = ['b', 'd', 'a', 'c', 'e']

    # When
    seedflow.run_sync(_async_func, _long(_log_a), _log_b, _log_c, _log_d, _log_e, log)

    # Then
    _assert(
        actual_log == expected_log,
        message="actual log != expected",
        actual_log=actual_log,
        expected=expected_log,
    )


def test_complex_flow_long_b_d() -> None:
    # Given
    actual_log = []

    def log(message):
        actual_log.append(message)

    expected_log = ['a', 'c', 'b', 'e', 'd']

    # When
    seedflow.run_sync(_async_func, _log_a, _long(_log_b), _log_c, _long(_log_d), _log_e, log)

    # Then
    _assert(
        actual_log == expected_log,
        message="actual log != expected",
        actual_log=actual_log,
        expected=expected_log,
    )


def test_complex_flow_long_c() -> None:
    # Given
    actual_log = []

    def log(message):
        actual_log.append(message)

    expected_log = ['a', 'b', 'd', 'c', 'e']

    # When
    seedflow.run_sync(_async_func, _log_a, _log_b, _long(_log_c), _log_d, _log_e, log)

    # Then
    _assert(
        actual_log == expected_log,
        message="actual log != expected",
        actual_log=actual_log,
        expected=expected_log,
    )


def test_complex_flow_long_d() -> None:
    # Given
    actual_log = []

    def log(message):
        actual_log.append(message)

    expected_log = ['a', 'b', 'c', 'e', 'd']

    # When
    seedflow.run_sync(_async_func, _log_a, _log_b, _log_c, _long(_log_d), _log_e, log)

    # Then
    _assert(
        actual_log == expected_log,
        message="actual log != expected",
        actual_log=actual_log,
        expected=expected_log,
    )


def test_complex_flow_long_e() -> None:
    # Given
    actual_log = []

    def log(message):
        actual_log.append(message)

    expected_log = ['a', 'b', 'c', 'd', 'e']

    # When
    seedflow.run_sync(_async_func, _log_a, _log_b, _log_c, _log_d, _long(_log_e), log)

    # Then
    _assert(
        actual_log == expected_log,
        message="actual log != expected",
        actual_log=actual_log,
        expected=expected_log,
    )


def test_run_async_func_runs_first_hunk() -> None:
    # Given
    expected_messages = ['async_func init w/arg and kwarg']
    actual_messages = []

    def record(message):
        actual_messages.append(message)

    async def async_func(arg, *, kwarg):
        record(f'async_func init w/{arg} and {kwarg}')
        return arg, kwarg

    # When
    seedflow.run_sync(async_func, 'arg', kwarg='kwarg')

    # Then
    _assert(
        expected_messages == actual_messages,
        message="messages recorded differs from expected",
        expected=expected_messages,
        actual=actual_messages,
    )


def test_run_sync_func_runs_func() -> None:
    # Given
    expected_messages = ['sync_func init w/arg and kwarg']
    actual_messages = []

    def record(message):
        actual_messages.append(message)

    def sync_func(arg, *, kwarg):
        record(f'sync_func init w/{arg} and {kwarg}')
        return arg, kwarg

    # When
    seedflow.run_sync(sync_func, 'arg', kwarg='kwarg')

    # Then
    _assert(
        expected_messages == actual_messages,
        message="messages recorded differs from expected",
        expected=expected_messages,
        actual=actual_messages,
    )


def test_spawn_returns_a_flow_id() -> None:
    # Given
    returned_id = None

    async def spawner(arg, *, kwarg):
        nonlocal returned_id
        returned_id = await seedflow.spawn(_async_nop)
        await seedflow.get_results()

    # When
    seedflow.run_sync(spawner, 'arg', kwarg='kwarg')

    # Then
    _assert(
        returned_id is not None,
        message="Returned ID is None!",
    )


def test_spawn_inits_an_async_func() -> None:
    # Given
    expected_hunks = ['1a']
    actual_hunks = []

    async def func(arg):
        actual_hunks.append('1a')

    async def spawner(arg, *, kwarg):
        returned_id = await seedflow.spawn(func, arg)
        results = await seedflow.get_results()
        return results[returned_id]

    # When
    seedflow.run_sync(spawner, 'arg', kwarg='kwarg')

    # Then
    _assert(
        expected_hunks == actual_hunks,
        message="The expected hunk was not recorded",
        expected_hunks=expected_hunks,
        actual_hunks=actual_hunks,
    )


def test_spawn_inits_a_sync_func() -> None:
    # Given
    expected_hunks = ['1a']
    actual_hunks = []

    def func(arg):
        actual_hunks.append('1a')

    async def spawner(arg, *, kwarg):
        returned_id = await seedflow.spawn(func, arg)
        results = await seedflow.get_results()
        return results[returned_id]

    # When
    seedflow.run_sync(spawner, 'arg', kwarg='kwarg')

    # Then
    _assert(
        expected_hunks == actual_hunks,
        message="The expected hunk was not recorded",
        expected_hunks=expected_hunks,
        actual_hunks=actual_hunks,
    )


def test_spawn_inits_an_erroring_async_func() -> None:
    # Given
    expected_error = seedflow.Error(None, RuntimeError('arg'), None)

    async def erroring_func(arg):
        raise RuntimeError(arg)

    async def spawner(arg, *, kwarg):
        returned_id = await seedflow.spawn(erroring_func, arg)
        results = await seedflow.get_results()
        return results[returned_id]

    # When
    results = seedflow.run_sync(spawner, 'arg', kwarg='kwarg')

    # Then
    _assert(
        results == expected_error,
        message="The expected error was not returned",
        returned=results,
        expected_error=expected_error,
    )


def test_spawn_inits_an_erroring_sync_func() -> None:
    # Given
    expected_error = seedflow.Error(None, RuntimeError('arg'), None)

    def erroring_func(arg):
        raise RuntimeError(arg)

    async def spawner(arg, *, kwarg):
        returned_id = await seedflow.spawn(erroring_func, arg)
        results = await seedflow.get_results()
        return results[returned_id]

    # When
    results = seedflow.run_sync(spawner, 'arg', kwarg='kwarg')

    # Then
    _assert(
        results == expected_error,
        message="The expected error was not returned",
        returned=results,
        expected_error=expected_error,
    )


def test_raising_an_error_from_top_async_func() -> None:
    # Given
    expected_error = RuntimeError('yo dere')
    actual_error = None

    async def func():
        raise RuntimeError('yo dere')

    # When
    try:
        seedflow.run_sync(func)
    except Exception as error:
        actual_error = error

    # Then
    _assert(
        # explicitly testing exact type
        type(actual_error) == type(expected_error) and actual_error.__dict__ == expected_error.__dict__,  # pylint: disable=unidiomatic-typecheck
        message="Error was not as expected",
        error=actual_error,
        expected=expected_error,
    )


def test_raising_an_error_from_top_sync_func() -> None:
    # Given
    expected_error = RuntimeError('yo dere')
    actual_error = None

    def func():
        raise RuntimeError('yo dere')

    # When
    try:
        seedflow.run_sync(func)
    except Exception as error:
        actual_error = error

    # Then
    _assert(
        # explicitly testing exact type
        type(actual_error) == type(expected_error) and actual_error.__dict__ == expected_error.__dict__,  # pylint: disable=unidiomatic-typecheck
        message="Error was not as expected",
        error=actual_error,
        expected=expected_error,
    )


def test_returning_with_active_spawned_subflows_errors() -> None:
    # Given
    expected_error_type = RuntimeError
    actual_error_type = None

    async def spawner(arg, *, kwarg):
        await seedflow.spawn(_async_nop)

    # When
    try:
        seedflow.run_sync(spawner, 'arg', kwarg='kwarg')
    except Exception as error:
        actual_error_type = type(error)

    # Then
    _assert(
        actual_error_type == expected_error_type,
        message="Error type didn't meet expectations",
        error_type=actual_error_type,
        expected_error_type=expected_error_type,
    )


def test_raising_with_active_spawned_subflows_errors() -> None:
    # Given
    expected_error_type = RuntimeError
    actual_error_type = None
    full_error = None

    async def spawner(arg, *, kwarg):
        await seedflow.spawn(_async_nop)
        raise RuntimeError('whoa dere')

    # When
    try:
        seedflow.run_sync(spawner, 'arg', kwarg='kwarg')
    except Exception as error:
        actual_error_type = type(error)
        full_error = error

    # Then
    _assert(
        actual_error_type == expected_error_type,
        message="Error type didn't meet expectations",
        error_type=actual_error_type,
        expected_error_type=expected_error_type,
        full_error=full_error,
    )


def test_returning_from_primary_async_func_returns() -> None:
    # Given
    expected_return = ('arg', 'kwarg')

    async def async_func(arg, *, kwarg):
        return arg, kwarg

    # When
    actual_return = seedflow.run_sync(async_func, 'arg', kwarg='kwarg')

    # Then
    _assert(
        expected_return == actual_return,
        message="return value differs from expected",
        expected=expected_return,
        actual=actual_return,
    )


def test_returning_from_primary_sync_func_returns() -> None:
    # Given
    expected_return = ('arg', 'kwarg')

    def sync_func(arg, *, kwarg):
        return arg, kwarg

    # When
    actual_return = seedflow.run_sync(sync_func, 'arg', kwarg='kwarg')

    # Then
    _assert(
        expected_return == actual_return,
        message="return value differs from expected",
        expected=expected_return,
        actual=actual_return,
    )


def test_returning_from_sub_async_func_returns_to_waiting_parent() -> None:
    # Given
    expected_return = ('arg', 'kwarg')

    async def sub_func(arg, *, kwarg):
        return arg, kwarg

    async def async_func(arg, *, kwarg):
        flow_id = await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        results = await seedflow.get_results()
        return results[flow_id].value

    # When
    actual_return = seedflow.run_sync(async_func, 'arg', kwarg='kwarg')

    # Then
    _assert(
        expected_return == actual_return,
        message="return value differs from expected",
        expected=expected_return,
        actual=actual_return,
    )


def test_returning_from_sub_sync_func_returns_to_waiting_parent() -> None:
    # Given
    expected_return = ('arg', 'kwarg')

    def sub_func(arg, *, kwarg):
        return arg, kwarg

    async def async_func(arg, *, kwarg):
        flow_id = await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        results = await seedflow.get_results()
        return results[flow_id].value

    # When
    actual_return = seedflow.run_sync(async_func, 'arg', kwarg='kwarg')

    # Then
    _assert(
        expected_return == actual_return,
        message="return value differs from expected",
        expected=expected_return,
        actual=actual_return,
    )


def test_returning_from_sub_async_func_does_not_return_to_non_waiting_parent() -> None:
    """
    Validate that if a subfunc raises, you still get the flow_id returned.

    It's possible that a raise would immediately be returned to the calling
    flow the next time it communicated with the runner.  Validate that this
    doesn't happen, and the flow id's returned are actually flow id's, rather
    than error output from a prior call.
    """
    # Given
    async def sub_func(arg, *, kwarg):
        return arg, kwarg

    async def async_func(arg, *, kwarg):
        await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        flow_2_id = await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        flow_3_id = await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        results = await seedflow.get_results()
        while len(results) < 3:
            results.update(await seedflow.get_results())
        return flow_2_id, flow_3_id

    # When
    actual_return = seedflow.run_sync(async_func, 'arg', kwarg='kwarg')

    # Then
    _assert(
        isinstance(actual_return[0], int),
        message="return value is not an int",
        value=actual_return,
    )
    _assert(
        isinstance(actual_return[1], int),
        message="return value is not an int",
        value=actual_return,
    )


def test_returning_from_sub_sync_func_does_not_return_to_non_waiting_parent() -> None:
    """
    Validate that if a subfunc raises, you still get the flow_id returned.

    It's possible that a raise would immediately be returned to the calling
    flow the next time it communicated with the runner.  Validate that this
    doesn't happen, and the flow id's returned are actually flow id's, rather
    than error output from a prior call.
    """
    # Given
    def sub_func(arg, *, kwarg):
        return arg, kwarg

    async def async_func(arg, *, kwarg):
        await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        flow_2_id = await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        flow_3_id = await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        results = await seedflow.get_results()
        while len(results) < 3:
            results.update(await seedflow.get_results())
        return flow_2_id, flow_3_id

    # When
    actual_return = seedflow.run_sync(async_func, 'arg', kwarg='kwarg')

    # Then
    _assert(
        isinstance(actual_return[0], int),
        message="return value is not an int",
        value=actual_return,
    )
    _assert(
        isinstance(actual_return[1], int),
        message="return value is not an int",
        value=actual_return,
    )


def test_raising_from_sub_async_func_does_not_return_to_non_waiting_parent() -> None:
    """
    Validate that if a subfunc raises, you still get the flow_id returned.

    It's possible that a raise would immediately be returned to the calling
    flow the next time it communicated with the runner.  Validate that this
    doesn't happen, and the flow id's returned are actually flow id's, rather
    than error output from a prior call.
    """
    # Given
    async def sub_func(arg, *, kwarg):
        raise RuntimeError(arg)

    async def async_func(arg, *, kwarg):
        await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        flow_2_id = await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        flow_3_id = await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        results = await seedflow.get_results()
        while len(results) < 3:
            results.update(await seedflow.get_results())
        return flow_2_id, flow_3_id

    # When
    actual_return = seedflow.run_sync(async_func, 'arg', kwarg='kwarg')

    # Then
    _assert(
        isinstance(actual_return[0], int),
        message="return value is not an int",
        value=actual_return,
    )
    _assert(
        isinstance(actual_return[1], int),
        message="return value is not an int",
        value=actual_return,
    )


def test_raising_from_sub_sync_func_does_not_return_to_non_waiting_parent() -> None:
    """
    Validate that if a subfunc raises, you still get the flow_id returned.

    It's possible that a raise would immediately be returned to the calling
    flow the next time it communicated with the runner.  Validate that this
    doesn't happen, and the flow id's returned are actually flow id's, rather
    than error output from a prior call.
    """
    # Given
    def sub_func(arg, *, kwarg):
        raise RuntimeError(arg)

    async def async_func(arg, *, kwarg):
        await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        flow_2_id = await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        flow_3_id = await seedflow.spawn(sub_func, arg, kwarg=kwarg)
        results = await seedflow.get_results()
        while len(results) < 3:
            results.update(await seedflow.get_results())
        return flow_2_id, flow_3_id

    # When
    actual_return = seedflow.run_sync(async_func, 'arg', kwarg='kwarg')

    # Then
    _assert(
        isinstance(actual_return[0], int),
        message="return value is not an int",
        value=actual_return,
    )
    _assert(
        isinstance(actual_return[1], int),
        message="return value is not an int",
        value=actual_return,
    )


def test_waiting_with_no_subflows_errors() -> None:
    # Given
    expected_error_type = RuntimeError
    actual_error_type = None
    full_error = None

    async def spawner(arg, *, kwarg):
        await seedflow.get_results()

    # When
    try:
        seedflow.run_sync(spawner, 'arg', kwarg='kwarg')
    except Exception as error:
        actual_error_type = type(error)
        full_error = error

    # Then
    _assert(
        actual_error_type == expected_error_type,
        message="Error type didn't meet expectations",
        error_type=actual_error_type,
        expected_error_type=expected_error_type,
        full_error=full_error,
    )


def test_unknown_request_errors() -> None:
    # Given
    expected_error_type = RuntimeError
    actual_error_type = None
    full_error = None

    @types.coroutine
    def unknown():
        return (yield 'HI THERE')

    async def spawner(arg, *, kwarg):
        await unknown()

    # When
    try:
        seedflow.run_sync(spawner, 'arg', kwarg='kwarg')
    except Exception as error:
        actual_error_type = type(error)
        full_error = error

    # Then
    _assert(
        actual_error_type == expected_error_type,
        message="Error type didn't meet expectations",
        error_type=actual_error_type,
        expected_error_type=expected_error_type,
        full_error=full_error,
    )


def test_till_all():
    # Given
    actual_hunks = []
    expected_hunks = [
        'A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1',
        'E2', 'F2', 'E3',
        'GA1', 'GB1', 'GB2',
    ]
    expected_results = (
        seedflow.Value('A1'),
        seedflow.Value('B2'),
        seedflow.Error(None, RuntimeError('C3'), None),
        seedflow.Value('D1'),
        seedflow.Value('E3'),
        seedflow.Error(None, RuntimeError('F2'), None),
        seedflow.Value((seedflow.Value('GA1'), seedflow.Value('GB2'))),
    )

    def sync_func(name, *, hunks, error=False):
        actual_hunks.append(f"{name}1")
        if error:
            raise RuntimeError(f"{name}{hunks}")
        return f"{name}{hunks}"

    async def async_func(name, *, hunks, error=False, sub_till=False):
        index = None
        for index in range(1, hunks + 1):
            await seedflow.run(actual_hunks.append, f"{name}{index}")
        if error:
            raise RuntimeError(f"{name}{index}")
        if sub_till:
            return await seedflow.till_all(
                seedflow.Task(sync_func, f'{name}A', hunks=1),
                seedflow.Task(async_func, f'{name}B', hunks=2),
            )
        return f"{name}{index}"

    async def example():
        return (await seedflow.till_all(
            seedflow.Task(sync_func, 'A', hunks=1),
            seedflow.Task(sync_func, 'B', hunks=2),
            seedflow.Task(sync_func, 'C', hunks=3, error=True),
            seedflow.Task(async_func, 'D', hunks=1),
            seedflow.Task(async_func, 'E', hunks=3),
            seedflow.Task(async_func, 'F', hunks=2, error=True),
            seedflow.Task(async_func, 'G', hunks=1, sub_till=True),
        ))

    # When
    results = seedflow.run_sync(example)

    # Then
    _assert(
        actual_hunks == expected_hunks,
        message="Hunks not run as expected",
        actual_hunks=actual_hunks,
        expected=expected_hunks,
    )
    _assert(
        results == expected_results,
        message="Results not as expected",
        results=results,
        expected_results=expected_results,
    )


def test_uncallable_spawn_errors() -> None:
    # Given
    expected_error_type = RuntimeError
    actual_error_type = None
    full_error = None

    # When
    try:
        seedflow.run_sync('not callable', 'arg', kwarg='kwarg')
    except Exception as error:
        actual_error_type = type(error)
        full_error = error

    # Then
    _assert(
        actual_error_type == expected_error_type,
        message="Error type didn't meet expectations",
        error_type=actual_error_type,
        expected_error_type=expected_error_type,
        full_error=full_error,
    )


def test_run_request():
    # Given
    # an awaitable func that awaits run
    async def _runs():
        result = await seedflow.run(print, 'testing')
        await seedflow.run(print, result)

    # When
    # the func is executed as a coro
    awaitable = _runs()
    # and the coro is progressed to the run call
    yielded = awaitable.send(None)

    # Then
    # the coro should yield a run request
    _assert(isinstance(yielded, seedflow.RunRequest), message="Expected RunRequest", got=type(yielded), value=yielded)
    # and the request should contain the args
    _assert(yielded.func == print, expected=print, got=yielded.func)
    _assert(yielded.args == ('testing',), expected=('testing',), got=yielded.args)
    _assert(yielded.kwargs == {}, expected={}, got=yielded.kwargs)

    # When
    # the coro is fed back a response
    second_yield = awaitable.send("test response")

    # Then
    # the coro should yield another run request which shows it handled
    #   the fed in response correctly.
    _assert(isinstance(second_yield, seedflow.RunRequest), message="Expected RunRequest", got=type(second_yield), value=second_yield)
    _assert(second_yield.args == ("test response",), expected=("test response",), got=second_yield.args)
    _assert(second_yield.kwargs == {}, expected={}, got=second_yield.kwargs)


def test_till_all_request():
    # Given
    # an awaitable func that awaits till all
    async def _waits_till_all():
        result = await seedflow.till_all(
            seedflow.Task(print, "first"),
            seedflow.Task(print, "second"),
        )
        await seedflow.run(print, result)

    # When
    # the func is executed as a coro
    awaitable = _waits_till_all()
    # and the coro is progressed to the till_all call
    yielded = awaitable.send(None)

    # Then
    # the coro should yield a till_all request
    _assert(isinstance(yielded, seedflow.TillAllRequest), expected=seedflow.TillAllRequest,
            got=type(yielded), value=yielded)
    _assert(yielded.tasks == (
        seedflow.Task(print, "first"),
        seedflow.Task(print, "second"),
    ), expected=(
        seedflow.Task(print, "first"),
        seedflow.Task(print, "second"),
    ), got=yielded.tasks)

    # When
    # The coro is fed back a response
    run_yield = awaitable.send('test response')

    # Then
    # the coro should yield a run request which shows it handled the fed in
    #   response correctly.
    _assert(isinstance(run_yield, seedflow.RunRequest), message="Expected RunRequest", got=type(run_yield), value=run_yield)
    _assert(run_yield.args == ("test response",), expected=("test response",), got=run_yield.args)
    _assert(run_yield.kwargs == {}, expected={}, got=run_yield.kwargs)


def test_till_any_request():
    # Given
    # an awaitable func that awaits till any
    async def _waits_till_any():
        result = await seedflow.till_any(
            seedflow.Task(print, "first"),
            seedflow.Task(print, "second"),
        )
        await seedflow.run(print, result)

    # When
    # the func is executed as a coro
    awaitable = _waits_till_any()
    # and the coro is progressed to the till_any call
    yielded = awaitable.send(None)

    # Then
    # the coro should yield a till_any request
    _assert(isinstance(yielded, seedflow.TillAnyRequest), expected=seedflow.TillAnyRequest,
            got=type(yielded), value=yielded)
    _assert(yielded.tasks == (
        seedflow.Task(print, "first"),
        seedflow.Task(print, "second"),
    ), expected=(
        seedflow.Task(print, "first"),
        seedflow.Task(print, "second"),
    ), got=yielded.tasks)

    # When
    # The coro is fed back a response
    run_yield = awaitable.send('test response')

    # Then
    # the coro should yield a run request which shows it handled the fed in
    #   response correctly.
    _assert(isinstance(run_yield, seedflow.RunRequest), message="Expected RunRequest", got=type(run_yield), value=run_yield)
    _assert(run_yield.args == ("test response",), expected=("test response",), got=run_yield.args)
    _assert(run_yield.kwargs == {}, expected={}, got=run_yield.kwargs)


def test_till_request():
    # Given
    # an awaitable func that awaits till a predicate
    def _predicate(*_tasks):
        # dummy predicate - not testing or even running this logic
        return True

    async def _waits_till():
        result = await seedflow.till(
            _predicate,
            seedflow.Task(print, "first"),
            seedflow.Task(print, "second"),
        )
        await seedflow.run(print, result)

    # When
    # the func is executed as a coro
    awaitable = _waits_till()
    # and the coro is progressed to the tillcall
    yielded = awaitable.send(None)

    # Then
    # the coro should yield a tillrequest
    _assert(isinstance(yielded, seedflow.TillRequest), expected=seedflow.TillRequest,
            got=type(yielded), value=yielded)
    _assert(yielded.predicate == _predicate, expected=_predicate, got=yielded.predicate)
    _assert(yielded.tasks == (
        seedflow.Task(print, "first"),
        seedflow.Task(print, "second"),
    ), expected=(
        seedflow.Task(print, "first"),
        seedflow.Task(print, "second"),
    ), got=yielded.tasks)

    # When
    # The coro is fed back a response
    run_yield = awaitable.send('test response')

    # Then
    # the coro should yield a run request which shows it handled the fed in
    #   response correctly.
    _assert(isinstance(run_yield, seedflow.RunRequest), message="Expected RunRequest", got=type(run_yield), value=run_yield)
    _assert(run_yield.args == ("test response",), expected=("test response",), got=run_yield.args)
    _assert(run_yield.kwargs == {}, expected={}, got=run_yield.kwargs)


def test_on_timeout_request():
    # Given
    # an awaitable func that awaits an on_timeout request
    _timeout = 10
    async def _on_timeout():
        result = await seedflow.on_timeout(
            _timeout,
            print, "first", "second", end='',
        )
        await seedflow.run(print, result)

    # When
    # the func is executed as a coro
    awaitable = _on_timeout()
    # and the coro is progressed to the on_interval call
    yielded = awaitable.send(None)

    # Then
    # the coro should yield a TimeoutRequest
    _assert(isinstance(yielded, seedflow.TimeoutRequest), expected=seedflow.TimeoutRequest,
            got=type(yielded), value=yielded)
    _assert(yielded.timeout == _timeout, expected=_timeout, got=yielded.timeout)
    _assert(yielded.func == print, expected=print, got=yielded.func)
    _assert(yielded.args == ("first", "second"), expected=("first", "second"), got=yielded.args)
    _assert(yielded.kwargs == {'end':''}, expected={'end':''}, got=yielded.kwargs)

    # When
    # The coro is fed back a response
    run_yield = awaitable.send('test response')

    # Then
    # the coro should yield a run request which shows it handled the fed in
    #   response correctly.
    _assert(isinstance(run_yield, seedflow.RunRequest), message="Expected RunRequest", got=type(run_yield), value=run_yield)
    _assert(run_yield.args == ("test response",), expected=("test response",), got=run_yield.args)
    _assert(run_yield.kwargs == {}, expected={}, got=run_yield.kwargs)


def test_on_interval_request():
    # Given
    # an awaitable func that awaits an on_interval request
    _interval = 10
    async def _on_interval():
        result = await seedflow.on_interval(
            _interval,
            print, "first", "second", end='',
        )
        await seedflow.run(print, result)

    # When
    # the func is executed as a coro
    awaitable = _on_interval()
    # and the coro is progressed to the on_interval call
    yielded = awaitable.send(None)

    # Then
    # the coro should yield a IntervalRequest
    _assert(isinstance(yielded, seedflow.IntervalRequest), expected=seedflow.IntervalRequest,
            got=type(yielded), value=yielded)
    _assert(yielded.interval == _interval, expected=_interval, got=yielded.interval)
    _assert(yielded.func == print, expected=print, got=yielded.func)
    _assert(yielded.args == ("first", "second"), expected=("first", "second"), got=yielded.args)
    _assert(yielded.kwargs == {'end':''}, expected={'end':''}, got=yielded.kwargs)

    # When
    # The coro is fed back a response
    run_yield = awaitable.send('test response')

    # Then
    # the coro should yield a run request which shows it handled the fed in
    #   response correctly.
    _assert(isinstance(run_yield, seedflow.RunRequest), message="Expected RunRequest", got=type(run_yield), value=run_yield)
    _assert(run_yield.args == ("test response",), expected=("test response",), got=run_yield.args)
    _assert(run_yield.kwargs == {}, expected={}, got=run_yield.kwargs)


import signal
def test_on_signal_request():
    # Given
    # an awaitable func that awaits an on_signal request
    _signal = signal.SIGINT
    async def _on_signal():
        result = await seedflow.on_signal(
            _signal,
            print, "first", "second", end='',
        )
        await seedflow.run(print, result)

    # When
    # the func is executed as a coro
    awaitable = _on_signal()
    # and the coro is progressed to the on_signal call
    yielded = awaitable.send(None)

    # Then
    # the coro should yield a SignalRequest
    _assert(isinstance(yielded, seedflow.SignalRequest), expected=seedflow.SignalRequest,
            got=type(yielded), value=yielded)
    _assert(yielded.signal == _signal, expected=_signal, got=yielded.signal)
    _assert(yielded.func == print, expected=print, got=yielded.func)
    _assert(yielded.args == ("first", "second"), expected=("first", "second"), got=yielded.args)
    _assert(yielded.kwargs == {'end':''}, expected={'end':''}, got=yielded.kwargs)

    # When
    # The coro is fed back a response
    run_yield = awaitable.send('test response')

    # Then
    # the coro should yield a run request which shows it handled the fed in
    #   response correctly.
    _assert(isinstance(run_yield, seedflow.RunRequest), message="Expected RunRequest", got=type(run_yield), value=run_yield)
    _assert(run_yield.args == ("test response",), expected=("test response",), got=run_yield.args)
    _assert(run_yield.kwargs == {}, expected={}, got=run_yield.kwargs)


def test_on_signal_forever_request():
    # Given
    # an awaitable func that awaits an on_signal request
    _signal = signal.SIGINT
    async def _on_signal_forever():
        result = await seedflow.on_signal_forever(
            _signal,
            print, "first", "second", end='',
        )
        await seedflow.run(print, result)

    # When
    # the func is executed as a coro
    awaitable = _on_signal_forever()
    # and the coro is progressed to the on_signal_forever call
    yielded = awaitable.send(None)

    # Then
    # the coro should yield a SignalForeverRequest
    _assert(isinstance(yielded, seedflow.SignalForeverRequest), expected=seedflow.SignalRequest,
            got=type(yielded), value=yielded)
    _assert(yielded.signal == _signal, expected=_signal, got=yielded.signal)
    _assert(yielded.func == print, expected=print, got=yielded.func)
    _assert(yielded.args == ("first", "second"), expected=("first", "second"), got=yielded.args)
    _assert(yielded.kwargs == {'end':''}, expected={'end':''}, got=yielded.kwargs)

    # When
    # The coro is fed back a response
    run_yield = awaitable.send('test response')

    # Then
    # the coro should yield a run request which shows it handled the fed in
    #   response correctly.
    _assert(isinstance(run_yield, seedflow.RunRequest), message="Expected RunRequest", got=type(run_yield), value=run_yield)
    _assert(run_yield.args == ("test response",), expected=("test response",), got=run_yield.args)
    _assert(run_yield.kwargs == {}, expected={}, got=run_yield.kwargs)


# XXX would like cleaner reasoning of hunk order
#     requires spawn to take varargs.
# XXX add testing for requests API - req_id = await request(flow_id, RequestType(*args, **kwargs))
#     messages = await get_messages()  # may be requests from other flows, or results from spawned flows or requests made
#     returning with outstanding messages results in an error
#     the current get_results API can persist - it just doesn't return requests.  possibly a get_requests that only gets requests?
# XXX requests from coros... vocab/wrapping needs work.  Weird that a rototiller.Returned is a "request" to seedflow's runner from a coroutine.
# XXX timeout should be smarter - some way to test that it's getting bogged down by making n time.sleep requests in threads?
# XXX signal and threads should be smarter - sync spawn funcs?
#     would spawn from the parent flow, results sent back to parent flow, but could be called synchronously from thread or signal callbacks
# XXX waiting for results should be smarter - it just checks and requeues right now
# XXX just releasing for someone else to run should be easier than `run(lambda: None)` - release? suspend? pause? run_others? tick? I like tick.
# XXX remove as many no coverage exceptions as possible
# XXX remove as many vulture exceptions as possible
# XXX remove as many nosec exceptions as possible
# XXX remove as many type: ignore exceptions as possible
# XXX remove as many noqa exceptions as possible
# XXX remove as many pylint exceptions as possible
# XXX remove duplicate tests
# XXX rename tests more consistently
# XXX simplify typing that is confusing
# XXX require some simple types
# XXX remove as many flake8 config exceptions as possible
# XXX remove as many pylint config exceptions as possible
# XXX try Black
# XXX try mutation testing
# XXX try fuzz testing
# XXX add testing for return type properties, equality, and repr
# XXX reorg seedflow to internal/API
# XXX reorg tests to internal/API
# XXX remove annotations future import if we don't need it
# XXX similar to trio - ALL exceptions should get propagated up a tree, rather than just up through the top-level run call.  Some special care probably needs to be taken to make sure that BaseException exceptions are just raised, rather than caught by the result encapsulation logic?
# XXX to ease adoption - have an async run func that yields out any unrecognized signals yielded at lower layers?
# XXX low-level suspend op?  instead of `await seedflow.run(lambda: None)`?
# XXX need a multiple signal handler test (several flows waiting for ctrlc)
# XXX on_signal_forever and on_interval as async iterators?
#     would mean they could skip intervals/signals if not re-awaited. could
#     avoid that with a queue. queue would grow forever if the iterator was not
#     re-awaited.  Could have them take iterators - would naturally return
#     when the iterator returned.  funcs could be wrapped in itertools.repeat?
#     No, this is the same problem, it just hides it.
#     We already have the missing events problem, we're just not paying attention
#     to it.  A queue with an optional max seems good.  Need duration/time
#     associated with each event/interval?  OOh - no, pass in number of events
#     since last trigger.  No queue, if I care about time I can get it myself.
#     might still want time for interval, since it's "free"?
#     I really like the iterator design idea:
"""
async for duration in seedflow.interval_stream(interval, max=3):
    # duration is the time from previous tick to this tick
async for signal in seedflow.signal_stream(*signals, max=3):
    # signal is the signal triggered

# body executed when interval ticks, or if there's already
# been a tick.
# if max exceeded, the iterator raises an exception - your code is not keeping up.
# max can be set to None, that's on you.
# provide a context manager version, so we can query it for length
async with seedflow.interval_stream(interval, max=300) as stream:
    len(stream)
# stream is closed on exit from context
"""
#     On the other hand, just triggering a spawn hits the "start thing on every event"
#     without worrying about queue size, or counting.  it's the least surprising.
#     It still achieves structure if the on_ func only returns when everything
#     completes.
#     on the other other hand, the user could spawn, too, from the iterator
# XXX need a smarter sleep - then we don't even need on-timeout
# XXX `with nursery` idea like trio for spawns?  `with spawn`?  Makes the
#     wait for spawned flows to complete explicit.
