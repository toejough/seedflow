"""Test runner for rototiller."""  # pylint: disable=invalid-name
# The module is just a script - having a hyphen in the name is fine.


# [ Imports:Python ]
import contextlib
import importlib
import importlib.util
import os
import pathlib
import sys
import types
import typing

# [ Imports:Third Party ]
import better_exceptions  # type: ignore
import blessed  # type: ignore
import click
import coverage  # type: ignore


def _import_module(name: str) -> types.ModuleType:
    path = pathlib.Path(name).resolve()
    module_name = pathlib.Path(name).stem
    spec = importlib.util.spec_from_file_location(module_name, str(path))
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    if spec.loader:
        spec.loader.exec_module(module)
    else:
        raise RuntimeError(f"Module {name} cannot be loaded!")
    return module


def _get_test_dir(test: typing.Callable[..., typing.Any]) -> str:
    filename = test.__code__.co_filename
    if not filename:
        raise RuntimeError(f"Cannot find the underlying file for {test}!")
    return str(pathlib.Path(filename).parent)


@contextlib.contextmanager
def _current_dir(path: str) -> typing.Iterator[None]:
    original = pathlib.Path.cwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(original)


def _run_test_in_dir(test: typing.Callable[[], typing.Any]) -> None:
    with _current_dir(_get_test_dir(test)):
        test()


def _report_not_implemented() -> None:
    print(blessed.Terminal().magenta("not yet implemented"))


def _handle_failure(error: AssertionError) -> typing.NoReturn:
    terminal = blessed.Terminal()
    print(terminal.bold_red("FAILED!"))
    if not str(error):
        error = terminal.red_on_white("No assertion message provided!!  (╯°□°)╯︵ ┻━┻ ...")
    print()
    print(error)
    sys.exit("early exit due to test failure")


def _report_error() -> None:
    print(blessed.Terminal().white_on_red("ERRORED!"))
    print()


def _report_pass() -> None:
    print(blessed.Terminal().bold_green("PASSED"))


def _report_test_run_summary(results: typing.Dict[str, int]) -> None:
    terminal = blessed.Terminal()
    num_total = results['total']
    num_passed = results['passed']
    num_failed = results['failed']
    num_ignored = results['ignored']
    num_errored = results['errored']
    num_not_run = num_total - num_passed - num_failed - num_ignored - num_errored
    passed = terminal.bold_green(str(num_passed)) if num_passed else "0"
    failed = terminal.bold_red(str(num_failed)) if num_failed else "0"
    ignored = terminal.magenta(str(num_ignored)) if num_ignored else "0"
    errored = terminal.white_on_red(str(num_errored)) if num_errored else "0"
    not_run = terminal.bold_white(str(num_not_run)) if num_not_run else "0"
    print(f"\nPassed: {passed} | Failed: {failed} | Ignored: {ignored} | Errored: {errored} | Not Run: {not_run}\n")


def _enable_better_exceptions() -> None:
    better_exceptions.hook()
    better_exceptions.MAX_LENGTH = 75
    # vulture whitelisting
    better_exceptions.MAX_LENGTH  # pylint: disable=pointless-statement


def _discover_test_funcs(test_module_name: str) -> typing.Dict[str, typing.Callable[[], typing.Any]]:
    this_module = _import_module(test_module_name)
    public_names = dir(this_module)
    test_names = (n for n in public_names if n.startswith('test_'))
    test_attrs = {n: getattr(this_module, n) for n in test_names}
    test_funcs = {n: t for n, t in test_attrs.items() if callable(t)}
    return test_funcs


def _report_discovered_tests(test_funcs: typing.Dict[str, typing.Callable[[], typing.Any]]) -> None:
    print(f"{len(test_funcs)} tests discovered...\n")


def _report_test_start(index: int, *, total: int, name: str) -> None:
    print(f"({index+1}/{total}) {name}: ", end='')
    sys.stdout.flush()


def _to_sections(numbers: typing.Iterable[int]) -> typing.Iterable[str]:
    section: typing.List[int] = []
    for this_number in numbers:
        if not section:
            section = [this_number]
            continue
        if this_number != section[-1] + 1:
            if len(section) == 1:
                yield f"{section[0]}"
            else:
                yield f"{section[0]}-{section[-1]}"
            section = [this_number]
        else:
            section.append(this_number)
    # final output
    if not section:
        return
    if len(section) == 1:
        yield f"{section[0]}"
    else:
        yield f"{section[0]}-{section[-1]}"


def _handle_coverage(target_coverage: coverage.Coverage) -> None:
    terminal = blessed.Terminal()
    data = target_coverage.get_data()
    files = data.measured_files()
    print("Coverage:")
    if not files:
        # XXX EARLY EXIT
        print("  No files analyzed for coverage!\n")
        sys.exit(1)

    total_missing_lines = 0
    for this_file in files:
        _, lines, missing_lines, _ = target_coverage.analysis(this_file)
        num_lines = len(lines)
        if not num_lines:
            continue
        num_missing = len(missing_lines)
        missing_sections = list(_to_sections(missing_lines))
        total_missing_lines += num_missing
        covered = num_lines - num_missing
        missing_text = f" | missing " + ", ".join(missing_sections)
        if num_missing:
            print(f"  {this_file}: {covered}/{num_lines} ({round(100*covered/num_lines, 0)}%){missing_text}")
        else:
            percent = terminal.green(f"{round(100*covered/num_lines, 0)}%")
            print(f"  {this_file}: {covered}/{num_lines} ({percent})")

    if total_missing_lines:
        print(f"  {total_missing_lines} missing line{'' if total_missing_lines == 1 else 's'}!\n")
        sys.exit(1)
    else:
        print()


@contextlib.contextmanager
def _coverage_measured() -> typing.Iterator[coverage.Coverage]:
    target_coverage = coverage.Coverage()
    target_coverage.start()
    try:
        yield target_coverage
    finally:
        target_coverage.stop()


def _warn_uncovered() -> None:
    terminal = blessed.Terminal()
    no_cover_lines = []
    for python_path in pathlib.Path.cwd().glob('**/*.py'):
        with python_path.open() as file:
            for index, line in enumerate(file):
                line = line.strip()
                if line.endswith("# pragma: no cover"):
                    no_cover_lines.append((python_path, index + 1))
                if line.endswith("# pragma: no coverage"):
                    no_cover_lines.append((python_path, index + 1))
    if no_cover_lines:
        print("Coverage Warning:")
        for path, index in no_cover_lines:
            print(terminal.magenta(f"  {str(path)}: {index}"))
        print(f"Found {len(no_cover_lines)} lines with coverage disabled!")
        print()


@click.command()
@click.argument('test-module-name')
def _run(test_module_name: str) -> None:
    _enable_better_exceptions()
    results = {
        'passed': 0,
        'failed': 0,
        'errored': 0,
        'ignored': 0,
        'total': 0,
    }
    try:
        with _coverage_measured() as target_coverage:
            test_funcs = _discover_test_funcs(test_module_name)
            _report_discovered_tests(test_funcs)

            results['total'] = len(test_funcs)
            for index, (name, test) in enumerate(test_funcs.items()):
                _report_test_start(index, total=len(test_funcs), name=name)
                try:
                    _run_test_in_dir(test)
                except NotImplementedError:
                    results['ignored'] += 1
                    _report_not_implemented()
                except AssertionError as error:
                    results['failed'] += 1
                    _handle_failure(error)
                except Exception:
                    results['errored'] += 1
                    _report_error()
                    raise
                else:
                    results['passed'] += 1
                    _report_pass()

    finally:
        _report_test_run_summary(results)

    _handle_coverage(target_coverage)
    _warn_uncovered()


if __name__ == "__main__":
    _run()
