"""Check the build."""  # pylint: disable=invalid-name
# this name is fine for a script


# [ Imports:Python ]
import functools
import json
import pathlib
import shutil
# bandit yells about subprocess, but we're using controlled input to it.
import subprocess  # nosec
import types
import typing

# [ Imports:Third Party ]
import click
import din
import pipe  # type: ignore


# this conforms to standards for generic type names
T = typing.TypeVar('T')  # pylint: disable=invalid-name


class Task(din.ReprMixin, din.FrozenMixin, din.EqualityMixin):
    """The base task that the seedflow runner executes."""

    def __init__(self, func: typing.Callable[..., typing.Any], *args: typing.Any, **kwargs: typing.Any) -> None:
        with self._thawed():
            super().__init__()
            self.func = func
            self.args = args
            self.kwargs = kwargs
            # Vulture
            # pylint: disable=pointless-statement
            self.func
            self.args
            self.kwargs
            # pylint: enable=pointless-statement


@types.coroutine
def run(func: typing.Callable[..., typing.Any], *args: typing.Any, **kwargs: typing.Any) -> typing.Generator[Task, T, T]:
    """Turn the given function and args into a task and yield it to the runner."""
    return (yield Task(func, *args, **kwargs))


@pipe.Pipe  # type: ignore
def _is_multiline(string: str) -> bool:
    return 1 < len(string.splitlines())


def _indent(string: str) -> str:
    return '  ' + '\n  '.join(string.splitlines())


def _limit_lines(string: str, *, max_lines: int) -> str:
    lines = string.splitlines()
    allowed_lines = lines[:max_lines]
    lines_left = len(lines) - len(allowed_lines)
    if lines_left:
        allowed_lines.append(f"<<<< +{lines_left} more >>>>")
    return '\n'.join(allowed_lines)


def _format_assertion(**message_kwargs: typing.Any) -> str:
    message = f"`Assertion failed!"
    for key, value in message_kwargs.items():
        value_string = f"{value}"
        if value_string | _is_multiline:
            value_string = _limit_lines(value_string, max_lines=20)
            value_string = "\n" + _indent(value_string)
        message += f"\n{key}: {value_string}"
    return message


def _raise_assertion(**message_kwargs: typing.Any) -> None:
    message = _format_assertion(**message_kwargs)
    raise AssertionError(message)


def _assert(success: bool, **message_kwargs: typing.Any) -> None:
    if not success:
        _raise_assertion(**message_kwargs)


def _sub(*args: str) -> subprocess.CompletedProcess:
    # bandit is angry, but we're doing subprocess calls very intentionally here
    return subprocess.run(args, capture_output=True, text=True)  # nosec


def _get_installed_packages_str() -> str:
    # sub runs with stdout = subprocess.PIPE and text mode, this is a string
    return typing.cast(str, _sub('pip', 'list', '--format', 'json').stdout)


def _get_installed_package_names() -> typing.List[str]:
    installed_packages_str = _get_installed_packages_str()
    installed_package_data = json.loads(installed_packages_str)
    return [d['name'] for d in installed_package_data]


def _install_wheel() -> None:
    _sub('pip', 'install', 'wheel').check_returncode()


def _ensure_wheel_installed() -> None:
    installed_package_names = _get_installed_package_names()
    if 'wheel' not in installed_package_names:
        _install_wheel()


def _build_distribution_files() -> None:
    _sub('python', 'setup.py', 'sdist', 'bdist_wheel').check_returncode()


def _get_possible_build_dirs() -> typing.Tuple[pathlib.Path, ...]:
    project_dir = pathlib.Path(__file__).parent
    build_dir = project_dir / 'build'
    dist_dir = project_dir / 'dist'
    egg_dirs = [e for e in project_dir.glob('*.egg-info') if e.is_dir()]
    return (build_dir, dist_dir, *egg_dirs)


def _get_expected_dist_paths(*, name: str, version: str) -> typing.Tuple[pathlib.Path, pathlib.Path]:
    project_dir = pathlib.Path(__file__).resolve().parent
    dist_dir = project_dir / 'dist'
    sdist_path = dist_dir / f'{name}-{version}.tar.gz'
    bdist_path = dist_dir / f'{name}-{version}-py3-none-any.whl'
    return (sdist_path, bdist_path)


def _build_package() -> None:
    _ensure_wheel_installed()
    _build_distribution_files()


def _with_teardown(
    teardown_func: typing.Callable[[], typing.Any],
) -> typing.Callable[[typing.Callable[..., T]], typing.Callable[..., T]]:
    def _decorator(func: typing.Callable[..., T]) -> typing.Callable[..., T]:
        @functools.wraps(func)
        def _wrapper(*args: typing.Any, **kwargs: typing.Any) -> T:
            try:
                return func(*args, **kwargs)
            finally:
                teardown_func()
        return _wrapper
    return _decorator


def _clean_out_build_artifacts() -> None:
    possible_build_dirs = _get_possible_build_dirs()
    existing_dirs = [d for d in possible_build_dirs if d.exists()]
    for directory in existing_dirs:
        shutil.rmtree(directory)


@click.command()
@click.option("--name", required=True, help="the package name")
@click.option("--version", required=True, help="the version")
@_with_teardown(_clean_out_build_artifacts)
def check_build(name: str, version: str) -> None:
    """
    Validate the tool can be built for deployment.

    * given any pre-existing build artifacts are cleaned out
    * when a build is run
    * the correct build artifacts exist
    """
    # Given
    _clean_out_build_artifacts()
    expected_dist_paths = _get_expected_dist_paths(name=name, version=version)

    # When
    _build_package()

    # Then
    for path in expected_dist_paths:
        _assert(path.exists(), missing_path=path)


# [ Script ]
if __name__ == '__main__':
    check_build()
